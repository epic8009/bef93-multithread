var code = []; //so code is a global
var stack = new Stack(); //stack is a global too

var direction = { //kind of useful way to do directions
    "NORTH": [0, -1],
    "EAST":  [1, 0],
    "SOUTH": [0, 1],
    "WEST":  [-1, 0]
}

function RNG(min,max) {
    return Math.round(Math.random()*(max-min))+min
}

function Stack() { //don't ask me why this wasnt just in the constructor, i didnt make it
    this.stack = [];
    return this;
}

Stack.prototype.push = function(value) {
    this.stack.push(value);
}

Stack.prototype.pop = function() {
    var a=this.stack.pop();
    return a===undefined?0:a;
}

Stack.prototype.add = function() {
    var x = this.pop();
    this.push(x + this.pop());
}

Stack.prototype.sub = function() {
    var x = this.pop();
    this.push(x - this.pop());
}

Stack.prototype.mul = function() {
    var x = this.pop();
    this.push(x * this.pop());
}

Stack.prototype.div = function() {
    var x = this.pop();
    this.push(Math.floor(x / this.pop()));
}

Stack.prototype.mod = function() {
    var x = this.pop();
    this.push(x % this.pop());
}

Stack.prototype.not = function() {
    this.push(!this.pop() ? 1 : 0);
}

Stack.prototype.greaterThan = function() {
    var x = this.pop();
    this.push(x < this.pop());
}

Stack.prototype.dup = function() {
    var x = this.pop();
    this.push(x);
    this.push(x);
}

Stack.prototype.swap = function() {
    var x = this.pop();
    var y = this.pop();
    this.push(x);
    this.push(y);
}

Stack.prototype.disc = function() {
    this.pop();
}

function Cursor(x, y, direction, stringmode) {
    this.x = x;
    this.y = y;
    this.direction = direction;
    this.stringmode = stringmode;
}

Cursor.prototype.getCode = function() {
    return code[this.y][this.x];
}

Cursor.prototype.move = function() {
    this.x += this.direction[0];
    this.y += this.direction[1]
}

Cursor.prototype.update = function() {
    if (this.stopped == true) {
        return "done";
    }
    this.moveNext=true;
    if (this.stringmode) {
        if (this.getCode() != '"') {
            stack.push(this.getCode().charCodeAt(0));
        } else {
            this.stringmode = !this.stringmode;
        }
    }
    if (!this.stringmode) {
        var c=this.getCode();
        if (c.charCodeAt()-0x30==parseInt(c,10)) { //detects if instruction is a number
            stack.push(parseInt(this.getCode(),10)); //if so, push that number to the stack
        }
        if (this.getCode() == "+") {
            stack.add();
        }
        if (this.getCode() == "-") {
            stack.sub();
        }
        if (this.getCode() == "*") {
            stack.mul();
        }
        if (this.getCode() == "/") {
            stack.div();
        }
        if (this.getCode() == "%") {
            stack.mod();
        }
        if (this.getCode() == "!") {
            stack.not();
        }
        if (this.getCode() == "`") {
            stack.greaterThan();
        }
        if (this.getCode() == "@") {
            this.stopped = true;
        }
        if (this.getCode() == ">") {
            this.direction = direction.EAST;
        }
        if (this.getCode() == "<") {
            this.direction = direction.WEST;
        }
        if (this.getCode() == "^") {
            this.direction = direction.NORTH;
        }
        if (this.getCode() == "v") {
            this.direction = direction.SOUTH;
        }
        if (this.getCode() == "?") {
            var rngOut = RNG(0,3)
            switch (rngOut) {
                case 0:
                    this.direction = direction.NORTH;
                break;
                case 1:
                    this.direction = direction.EAST;
                break;
                case 2:
                    this.direction = direction.WEST;
                break;
                case 3:
                    this.direction = direction.SOUTH;
                break;
            }
        }
        if (this.getCode() == "_") {
            if (stack.pop()==0 || stack.pop()===undefined) {
                this.direction=direction.EAST;
            } else {
                this.direction=direction.WEST;
            }
        }
        if(this.getCode() == "|") {
            if (stack.pop()==0 || stack.pop()===undefined) {
                this.direction=direction.SOUTH;
            } else {
                this.direction=direction.NORTH;
            }
        }
        if(this.getCode() == ":") {
            var x=stack.pop();
            if (x===undefined){
                stack.push(0);
            }
            stack.push(x);
            stack.dup();
        }
        
    }

    if (this.moveNext) { //we don't always want to move after an instruction, like #
        this.move();
    }
}

function setLength(string, length) { //forces a string to be a certain length, fills with spaces if neccessary
    if (string.length >= length) {
        return string;
    } else {
        return string + new Array(length - string.length + 1).join(' ');
    }
}

function format(id) { //turns the input into an array
    var element = document.getElementById("input")
    var input = element.value || element.textContent || element.innerText;
    var code = input.split("\n");
    var maxLength = Math.max.apply(null, code.map(function x(str){ return str.length; }));
    for (var i = 0; i < code.length; i++) {
        code[i] = setLength(code[i], maxLength);
    }
    return code;
}

function startprog() {
    code = format("input");
    console.log(code);
    console.log("Finished parsing code.");
    var cursor = new Cursor(0, 0, direction.EAST, false);
    for (var i = 0; i < 256; i++) {
        cursor.update();
    }
    return cursor;
}

// test
